# mhs-rest-api

REST API for [mhs-web-ui](https://bitbucket.org/mhsholdings2/mhs-web-ui) built on [SailsJS](http://sailsjs.org)

# Pre-requisites
[node](https://nodejs.org) and [npm](https://nodejs.org)

# Run for Development
1. `npm install -g sails`
1. `npm install`
2. `sails lift` to start server
3. API runs at `http://localhost:1337`

# Run for Production
1. `npm start --prod`
2. API runs at `http://domain:3000` by default. Port can be changed by specifying environment variable `PORT`

# DB Connections
- DB connections located at `config/connections.js`
- `config/env/development.js` and `config/env/production.js` used for selecting DB connection
- In Prod, DB connection reads from environment variables:
 - DB_HOSTNAME
 - DB_USER
 - DB_PASSWORD
 - DB_DATABASE_NAME
 
# Elastic Beanstalk config
- `.elasticbeanstalk/config.yml` used to define EB Environment and EB App Name
- `.ebextensions/nodecommand.config` used to define `npm start --prod` as the start command in EB
- `.ebextensions/options.config` used to define environment variables and proxy server
