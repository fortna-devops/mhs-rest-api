/**
 * AlertsController
 *
 * @description :: Server-side logic for managing alerts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	getRandomAlert: function(req, res) {
        return res.json({ results: alerts.getRandomAlert() });
    },

	getAlerts: function(req, res) {
        return res.json({ results: alerts.getAlerts() });
    }    
};

