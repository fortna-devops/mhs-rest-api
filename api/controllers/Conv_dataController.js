/**
 * Conv_dataController
 *
 * @description :: Server-side logic for managing conv_datas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    getChartData: function (req, res) {
        var conveyor = req.param('conveyor');
        var timestamp_start = req.param('timestamp_start');
        var timestamp_end = req.param('timestamp_end');
        var limit = req.param('limit');

        if(!conveyor||!timestamp_start||!timestamp_end){
            
            res.serverError({message:'Missing param. Mandatory params: conveyor, timestamp_start, timestamp_end'});
            return;
        }
        if(!limit){
            limit=1000;
        }

        // timestamp_start = new Date(timestamp_start);
        // timestamp_end = new Date(timestamp_end);

        console.log(conveyor)
        console.log(timestamp_start)
        console.log(timestamp_end)

        var conv_table = process.env.DB_CONV_TABLE||'conv_data';

        var query = 'SELECT datetime,conveyor,speed,amps,rate FROM '+conv_table+' WHERE datetime>= $1 AND datetime<$2 AND priority=1 AND conveyor=$3';
        var queryParams = [timestamp_start, timestamp_end, conveyor];

        query+=' ORDER BY datetime'
        Conv_data.query(query, queryParams, function (err, results) {
            if (err) { return res.serverError(err); }

            return res.ok({ results: results.rows });
        });

    },
    getChartDataMongo: function (req, res) {

        // Conv_data.find({ timestamp: { '>': new Date('12-24-2017 00:00:00'), '<': new Date('01-10-2018 00:00:00') } }).exec(function (err, results){
        //     console.log(results)
        //     return res.json({ results: results });
        // });
        var conveyor = req.param('conveyor');
        var timestamp_start = req.param('timestamp_start');
        var timestamp_end = req.param('timestamp_end');
        var limit = req.param('limit');

        if(!conveyor||!timestamp_start||!timestamp_end){
            
            res.serverError({message:'Missing param. Mandatory params: conveyor, timestamp_start, timestamp_end'});
            return;
        }
        if(!limit){
            limit=1000;
        }

        // timestamp_start=timestamp_start.replace(' ','T');
        // timestamp_end=timestamp_end.replace(' ','T');
        // timestamp_start+='.000Z';
        // timestamp_end+='.000Z';
        timestamp_start=new Date(timestamp_start);
        timestamp_end=new Date(timestamp_end);

        console.log(conveyor)
        console.log(timestamp_start)
        console.log(timestamp_end)
        
        Conv_data.native(function (err, collection){
            if(err)
                return res.serverError(err);
            
            collection.aggregate(
                [
                    {$match:{conveyor:conveyor,
                        datetime: {
                            $gte: timestamp_start,
                            $lt: timestamp_end
                        }
                    
                        }
                    },
                    {$sample:{size:limit}},
                    {$sort:{datetime:1}},
                    {$project:{_id:0,datetime:1,conveyor:1,speed:1,amps:1,rate:1}}
                ])
            
            .toArray(function(err, results){
                if(err)
                    return res.serverError(err);
                
                
                // console.log(results);
                    
                return res.ok({results:results});
            });

        });

    },
};

