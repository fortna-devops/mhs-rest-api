/**
 * Sensor_dataController
 *
 * @description :: Server-side logic for managing conv_datas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    getChartData: function (req, res) {
        var conveyor = req.param('conveyor');
        var part = req.param('part');
        var location = req.param('location');
        var timestamp_start = req.param('timestamp_start');
        var timestamp_end = req.param('timestamp_end');
        var limit = req.param('limit');

        if (!conveyor || !timestamp_start || !timestamp_end) {

            res.serverError({ message: 'Missing param. Mandatory params: conveyor, timestamp_start, timestamp_end' });
            return;
        }
        if (!limit) {
            limit = 1000;
        }

        // timestamp_start = new Date(timestamp_start);
        // timestamp_end = new Date(timestamp_end);

        console.log(conveyor)
        console.log(timestamp_start)
        console.log(timestamp_end)
        var sensors_table = process.env.DB_SENSORS_TABLE||'sensors_data';

        var query = 'SELECT datetime,temp_f,x_rms_v_ips,z_rms_ips,conveyor,part,location FROM '+sensors_table+' WHERE datetime>= $1 AND datetime<$2 AND priority=1 AND conveyor = $3';
        var queryParams = [timestamp_start, timestamp_end, conveyor];

        if (part) {
            query += ' AND part=$4';
            queryParams.push(part);

            if (location) {
                query += ' AND location=$5';
                queryParams.push(location);
            }
        } else if (location) {
            query += ' AND location=$4';
            queryParams.push(location);
        }
        query+=' ORDER BY datetime'
        Sensor_data.query(query, queryParams, function (err, results) {
            if (err) { return res.serverError(err); }

            return res.ok({ results: results.rows });
        });

    },
    getChartDataMongo: function (req, res) {

        // Conv_data.find({ timestamp: { '>': new Date('12-24-2017 00:00:00'), '<': new Date('01-10-2018 00:00:00') } }).exec(function (err, results){
        //     console.log(results)
        //     return res.json({ results: results });
        // });
        var conveyor = req.param('conveyor');
        var part = req.param('part');
        var location = req.param('location');
        var timestamp_start = req.param('timestamp_start');
        var timestamp_end = req.param('timestamp_end');
        var limit = req.param('limit');

        if (!conveyor || !timestamp_start || !timestamp_end) {

            res.serverError({ message: 'Missing param. Mandatory params: conveyor, timestamp_start, timestamp_end' });
            return;
        }
        if (!limit) {
            limit = 1000;
        }

        // timestamp_start=timestamp_start.replace(' ','T');
        // timestamp_end=timestamp_end.replace(' ','T');
        // timestamp_start+='.000Z';
        // timestamp_end+='.000Z';
        timestamp_start = new Date(timestamp_start);
        timestamp_end = new Date(timestamp_end);

        console.log(conveyor)
        console.log(timestamp_start)
        console.log(timestamp_end)

        Sensor_data.native(function (err, collection) {
            if (err)
                return res.serverError(err);


            var matchObj = {
                conveyor: conveyor,
                datetime: {
                    $gte: timestamp_start,
                    $lt: timestamp_end
                }
            }

            if (part) {
                matchObj.part = part;
            }

            if (location) {
                matchObj.location = location;
            }

            collection.aggregate(
                [
                    {
                        $match: matchObj
                    },
                    { $sample: { size: limit } },
                    { $sort: { datetime: 1 } },
                    { $project: { _id: 0, datetime: 1, Z_RMS_IPS: 1, X_RMS_V_IPS: 1, TEMP_F: 1 } }//conveyor:1,part:1,location:1,
                ])

                .toArray(function (err, results) {
                    if (err)
                        return res.serverError(err);


                    // console.log(results);

                    return res.ok({ results: results });
                });

        });

    },
};

