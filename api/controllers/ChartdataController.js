/**
 * ChartdataController
 *
 * @description :: Server-side logic for managing chartdatas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
    getRandomChartData: function(req, res) {
        return res.json({ results: charts.getRandomChartData() });
    },

    getChartData: function(req, res) {
        return res.json({ results: charts.getChartData() });
    }

};

