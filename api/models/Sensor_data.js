/**
 * Sensor_data.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    datetime:{
      type:'datetime'
    },

    conveyor:{
      type:'string'
    },

    location:{
      type:'string'
    },

    part:{
      type:'string'
    },

    temp_f:{
      type:'float'
    },
    
    z_rms_ips:{
      type:'float'
    },

    x_rms_v_ips:{
      type:'float'
    },

    createdAt: {
      type: 'datetime',
      columnName: 'created_at'
    },
    updatedAt: {
      type: 'datetime',
      columnName: 'updated_at'
    }
  }
};

