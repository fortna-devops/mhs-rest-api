/**
 * Conv_data.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    timestamp: {
      type: 'datetime'
    },

    conveyor: {
      type: 'string'
    },

    speed: {
      type: 'float'
    },
    amps: {
      type: 'float'
    },
    rate: {
      type: 'float'
    },
    createdAt: {
      type: 'datetime',
      columnName: 'created_at'
    },
    updatedAt: {
      type: 'datetime',
      columnName: 'updated_at'
    }
  }
};

