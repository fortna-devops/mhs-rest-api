
var alerts = require('../../assets/json/mhs_alerts.json');

module.exports = {

	getRandomAlert: function(req, res) {
	  var totalAmount = alerts.results.length;
	  var rand = Math.floor(Math.random() * totalAmount);
	  return alerts.results[rand];
    },

    getAlerts: function(req, res) {
	  return alerts.results;
    }

};