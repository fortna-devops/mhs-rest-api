
var chartdata = require('../../assets/json/mhs_chart_data.json');

module.exports = {

	getRandomChartData: function(req, res) {
	  var totalAmount = chartdata.results.length;
	  var rand = Math.floor(Math.random() * totalAmount);
	  return chartdata.results[rand];
    },

    getChartData: function(req, res) {
	  return chartdata.results;
    }

};